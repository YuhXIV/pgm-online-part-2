# PGM Online Part 2

### Task 25
- [x] Upgrade you PGM to allow for the user to enter and view Supervisors
- [x] Use a temporary data store as per the tutorial
- [x] The form should have full validation
- [x] Use Bootstrap to make it look good - Experiment

### Task 26
- [x] Upgrade your supervisor class to have the ability to use null
conditional operators. Demonstrate this working any way
you want
- [x] Additionally, include default values for properties in the
supervisor class
- [x] Create a method in Supervisors which generates and
returns a List of 4 supervisors
- [x] Create a view to display this list but only if their name
starts with �S� (use a lambda expression in the controller)
42
