﻿using PGMOnlinePartTwo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PGMOnlinePartTwo.Controllers
{
    public static class SupervisorGroup
    {
        private static List<Supervisor> supervisors = new List<Supervisor>();
        private static int seed = 1;
        public static List<Supervisor> Supervisors
        {
            get { return supervisors; }
        }

        public static void AddSupervisor(Supervisor supervisor)
        {
            supervisors.Add(supervisor);
        }

        public static void GenRandomSupervisor()
        {
            for(int i = 0; i < 4; i++)
            {
                Supervisor supervisor = new Supervisor()
                {
                    Id = seed,
                    Name = GenName(seed++),
                    IsAvailable = false,
                    Level = "Noob",
                };
                supervisors.Add(supervisor);
            }
        }

        private static string GenName(int seed)
        {
            string[] names = {"Peter", "Thomas", "Stephen", "Orjan", "Markus", "David", "Tommy", "Christopher",
                "Nier", "Noah", "Yuh", "Garvin", "Neah", "Niel", "Anna", "Nina", "Yuna", "Yuki", "Lena", "Lina", "Simen",
                "Sarah", "Anja", "Eva", "Vanessa", "Stine", "Charlotte", "Lene", "Jonas", "Dennis", "John", "Jason",
                "Jack", "Tom", "Annie", "Melody", "Marilyn", "Nelson", "Charlie", "Joseph", "Oscar", "Emile", "Taylor"};

            return names[new Random(seed).Next(names.Length - 1)];
        }
    }
}
