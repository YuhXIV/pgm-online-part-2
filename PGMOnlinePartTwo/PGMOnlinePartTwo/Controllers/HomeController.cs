﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PGMOnlinePartTwo.Models;

namespace PGMOnlinePartTwo.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("PGMOnline");
        }
        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            return View(SupervisorGroup.Supervisors);
        }
        /// <summary>
        /// Adding new supervisor
        /// </summary>
        /// <param name="supervisor"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult PGMOnline(Supervisor supervisor)
        {
            if(ModelState.IsValid)
            {
                SupervisorGroup.AddSupervisor(supervisor);
                return View("AddSupervisorConfirmation", supervisor);
            }
            return View();
            
        }
        /// <summary>
        /// display all the supervisor
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult AllSupervisor()
        {
            return View(SupervisorGroup.Supervisors);
        }
        public IActionResult GenerateRandomSupervisors()
        {
            SupervisorGroup.GenRandomSupervisor();
            return View("AllSupervisor", SupervisorGroup.Supervisors);
        }
        public IActionResult ListWithS()
        {
            return View(SupervisorGroup.Supervisors.Where(s => s.Name.Substring(0,1).ToLower().Equals("s")));
        }
    }
}
