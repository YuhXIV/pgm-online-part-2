﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PGMOnlinePartTwo.Models
{
    public class Supervisor
    {
        //[Required(ErrorMessage = "Please enter a number")]
        //[RegularExpression("^\\d+$", ErrorMessage = "Please enter in a valid whole number")]
        public int? Id { get; set; } = new Random(DateTime.Now.Second).Next(1, 60);
        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; } = "Unknown";
        //[Required(ErrorMessage = "Please choose a availability")]
        public bool? IsAvailable { get; set; } = false;
        [Required(ErrorMessage = "Please enter a level")]
        public string Level { get; set; } = "Noob";

    }
}
